<?php 

// overrides the default PHP memory limit.
set_time_limit(0);
ini_set('upload_max_filesize', '1500M');
ini_set('post_max_size', '1500M');
ini_set('max_input_time', 4000); // Play with the values
ini_set('max_execution_time', 4000); // Play with the values

ini_set('memory_limit', '-1');
// overrides the default PHP memory limit.

// include pdo helper class to use common methods
ini_set("display_startup_errors", 1);
ini_set("display_errors", 1);
// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

// include pdo helper class to use common methods
//error_reporting(E_ALL ^ E_DEPRECATED);

if($_SERVER['HTTP_HOST']=="localhost"){
	
	define('HOST',"localhost");
	define('USER',"root");
	define('PASSWORD',"");
	define('DB',"barkha");
}else{

	define('HOST',"localhost");
	define('USER',"ourteam_tester");
	define('PASSWORD',"tester@123");
	define('DB',"ourteam_tester");
}
	
define('TITLE',"SDP Master");
define('CLASS_D',"SDP Master");
define('CONFIG_D',"SDP Master");
	
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')           
    define("DS", "\\");
else 
    define("DS", "/");
$path= __DIR__; 
$parray= explode(DS,$path);
//unset ($parray[count($parray)-1]);

$path= implode(DS,$parray);

$cpath= implode(DS,$parray);	
if($_SERVER['HTTP_HOST']=="localhost"){
	define('URL_PATH',"http://localhost/barkha_prj/");
	define('CG_PATH',"http://localhost/barkha_prj/c/");
	define("CONTENT_PATH",$cpath.DS."content".DS);
	define("CONTENT_URL_PATH",URL_PATH."../content");
}else{
	unset ($parray[count($parray)-1]);

	$cpath= implode(DS,$parray);	
	define('URL_PATH',"http://ourteam.co.in/barkha_prj/");
	define('CG_PATH',"http://ourteam.co.in/barkha_prj/c/");
	define("CONTENT_PATH",$cpath.DS."content".DS);//die($cpath);
	define("CONTENT_URL_PATH",URL_PATH."../../content");
}
define('APP_PATH',$path);
define('CLASS_PATH',APP_PATH.DS."class");
define('CONFIG_PATH',APP_PATH.DS."config");
define('TEMPLATE_PATH',APP_PATH.DS."template");

require_once('class/function.php');
require_once('class/mycurl.php');

?>