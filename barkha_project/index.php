<?php
 //300 seconds = 5 minutes execution time
ini_set('max_execution_time', 300);
// overrides the default PHP memory limit.
ini_set('memory_limit', '-1');

// include pdo helper class to use common methods
error_reporting(E_ALL);
// Report all PHP errors
error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
include("config.php");
global $fn;

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Hindi Stastical Analytics</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="css/bootstrap-table.css" rel="stylesheet" type="text/css"/>
	<link href="css/kendo.common.min.css" rel="stylesheet"/>
	<link href="css/kendo.bootstrap.min.css" rel="stylesheet"/>
	<link href="css/gv.bootstrap-form.css" rel="stylesheet" type="text/css"/>
	<style>
		.navbar {
			margin-bottom: 0;
			border-radius: 0;
		}
		
		footer {
			background-color: #f2f2f2;
			padding: 25px;
		}
	</style>
</head>

<body>
	<div class="jumbotron">
		<div class="container text-center">
			<h1>Search Form</h1>
			<p></p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<form method="post" action="submit.php" id="formentry" class="form-horizontal" role="form" data-parsley-validate novalidate>
				<div class="container-fluid shadow">
					<div class="row">
						<div id="valErr" class="row viewerror clearfix hidden">
							<div class="alert alert-danger">Oops! Seems there are some errors..</div>
						</div>
						<div id="valOk" class="row viewerror clearfix hidden">
							<div class="alert alert-success">Yay! ..</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">

									<fieldset>
										<div class="col-md-10">
											<div class="form-group">
												<div class="controls col-sm-12">
													<input id="field23" name="url_submit" placeholder="URL" type="text" class="form-control k-textbox" data-role="text" data-parsley-errors-container="#errId1"><span id="errId1" class="error"></span>
												</div>
											</div>
											<div class="form-group">
												<div class="controls col-sm-12">
													<select id="field24" placeholder="Result" class="form-control" data-role="select" data-parsley-errors-container="#errId2" multiple>
														<option value="">Result</option>
													</select><span id="errId2" class="error"></span>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<button id="button32" type="reset" class="col-md-12 btn btn-default">Clear</button>
											</div>
											<div class="form-group">
												<button id="button34" type="submit" class="col-md-12 btn btn-default">Add Word</button>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<fieldset>
										<div class="col-md-10">
											<div class="form-group">
												<div class="controls col-sm-12">
													<select id="field37" class="form-control" multiple data-role="select" data-parsley-errors-container="#errId3">
														<option value="">Word List</option>
													</select><span id="errId3" class="error"></span>
												</div>
											</div>
											<div class="form-group">
												<div class="controls col-sm-12">
													<input id="field38" placeholder="Filter" type="text" class="form-control k-textbox" data-role="text" data-parsley-errors-container="#errId1"><span id="errId1" class="error"></span>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<button id="button39" type="button" class="col-md-12 btn btn-default">Add Word</button>
											</div>
											<div class="form-group">
												<button id="button43" type="button" class="col-md-12 btn btn-default">Delete Word</button>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-9">
										<div class="form-group">
											<div class="controls col-sm-12">
												<select id="field50" class="form-control ui fluid dropdown" data-role="select" data-parsley-errors-container="#errId4">
													<option value="">Statistics</option>
												</select><span id="errId4" class="error"></span>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<button id="button47" type="button" class="col-md-12 btn btn-default">Calculate</button>
										</div>
										<div class="form-group">
											<button id="button49" type="button" class="col-md-12 btn btn-default">Clear</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							<table style="width: 100%;" border="1">
							  <tbody>
								<tr>
								  <th style="text-align: center;" scope="col">Id</th>
								  <th style="text-align: center;" scope="col">Word</th>
								  <th style="text-align: center;" scope="col">Length</th>
								  <th style="text-align: center;" scope="col">Count As Practical Word</th>
								  <th style="text-align: center;" scope="col">Count As Dictonary Word</th>
								</tr>
								<tr>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								</tr>
							  </tbody>
							</table>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<footer class="container text-center">
		<p></p>
	</footer>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/bootstrap-table.js" type="text/javascript"></script>
	<script src="js/jquery.inputmask.js" type="text/javascript"></script>
	<script src="js/kendo.all.min.js"></script>
	<script src="js/parsley.extend.js" type="text/javascript"></script>
	<script src="js/parsley.js" type="text/javascript"></script>
	<script src="js/download.js" type="text/javascript"></script>
	<script src="js/protostrap.js" type="text/javascript"></script>
</body>

</html>