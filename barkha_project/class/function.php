<?php

/**
 * @author Vishal Dubey
 * @copyright 2015
 */  
session_start();

	$path= __DIR__; 

if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')           
    define("DS1", "\\");
else 
    define("DS1", "/");
	
	$parray= explode(DS1,$path);
	unset ($parray[count($parray)-1]);
	$path1= implode(DS1,$parray);

// include pdo helper class to use common methods
include( CLASS_PATH.DS1.'class.pdohelper.php');
// include pdo class wrapper
include( CLASS_PATH.DS1.'class.pdowrapper.php');

//include ("mailer/PHPMailerAutoload.php");
include(CLASS_PATH.DS1."menus.php");

	 $host=HOST;
     $username=USER;    // specify the sever details for mysql
     $password=PASSWORD;
     $database=DB;
     global $db, $conn;
	// database connection setings
	$dbConfig = array("host"=>$host, "dbname"=>$database, "username"=>$username, "password"=>$password);
	// get instance of PDO Wrapper object
	$db = new PdoWrapper($dbConfig);
	
	// get instance of PDO Helper object
	$helper = new PDOHelper();
	
	// set error log mode true to show error on screen or false to log in log file
	$db->setErrorLog(true);

class functions
{
	function __construct(){
	}
	function ins_jquery()
	{?>
		 <script src="<?php echo PATH; ?>js/jquery.js"></script>
	<?php }
	function modal_window($args=array()){
	?>
	<!-- Modal -->
	<div class="modal fade" id="<?php echo $args['name'] ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $args['name'] ?>Label" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="<?php echo $args['title'] ?>" id="<?php echo $args['name'] ?>Label"><?php echo $args['title'] ?></h4>
		  </div>
		  <div class="modal-body">
			<?php echo $args['str'];?>
		  </div>
		</div>
	  </div>
	</div>
	<?php
	}
	
	function selectData($sTable , $aColumn, $aWhere, $sOther,$debug=0)	{
		global $db;
		$data = $db->select($sTable , $aColumn, $aWhere, $sOther)->results(); 
		if($debug==1){$data[]=$db->showQuery(); }
		return $data;
	}
	
	public static function raw_selectData($sql,$debug=0){
		global $db;
		$data = $db->pdoQuery($sql)->results(); 
		if($debug==1){$data[]=$db->showQuery(); }
		return $data;
		
	}
	
	public static function raw_selectData_type($sql,$type="",$debug=0){
		global $db;
		$data = $db->pdoQuery($sql)->showQuery()->results($type);
		return $data;
		
	}
	function addData($table, $dataArray,$debug=0) {
		global $db;
		$q[] = $db->insert($table,$dataArray)->getLastInsertId();
		if($debug==1){$q[]=$db->showQuery(); }
		
		return $q;
		
	}
	function deleteData($tablename,$where,$debug=0){
		global $db;
		//$p->delete($tablename, $aWhere)->affectedRows(); 
		$q = $db->delete($tablename, $where)->affectedRows();
		// print affected rows
		if($debug==1){$data[]=$db->showQuery(); }
	}

	function updateData($tablename,$var_str,$where,$debug=0){
		global $db;
		$q = $db->update($tablename, $var_str, $where)->affectedRows();
		// print affected rows
		if($debug==1){$data[]=$db->showQuery(); return $data; }
	}
	function mysql_date($date_str)
	{
		$dob1 = date("Y-m-d", strtotime($date_str));//2015-06-16 18:03:13
		return($dob1);
	}
	function limitdata($string,$limit){
		global $conn;
		$str = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $limit+1));
		
		return $str."... ";
	}

	function checkLogin(){
		if(!isset($_SESSION['uname']) && !isset($_SESSION['password'])){
			return false;
		}
		return true;
	}
	function show_alert(){
		if(@$_SESSION['err']!=""){
			echo "<br>";
			echo '<div class="alert alert-danger alerthide" role="alert">'.$_SESSION['err'].'</div>';
			unset($_SESSION['err']);
		}
		if(@$_SESSION['msg']!=""){
			echo "<br>";
			echo '<div class="alert alert-success alerthide" role="alert">'.$_SESSION['msg'].'</div>';
			unset($_SESSION['msg']);
		}
	}
	function loginAdmin(){
		global $conn;
		$uname = $_POST['uname'];
		$password = $_POST['password'];
		$sql= "Select * from `v_users_master` where (`users_username` = '".$uname."' OR `users_email` = '".$uname."') AND `users_pwd` = '".$password."' ";
		 //echo $sql; 
        $row = self::raw_selectData($sql,0);
		//die(count($row)."here");
		if(count($row) != 0 ) {
			//echo "<pre>";
			//print_r($row);
			//echo "</pre>";
			$_SESSION['u_idsdp'] = $row[0]['users_username'];
			$_SESSION['email'] = $row[0]['users_email'];
			$_SESSION['id'] = $row[0]['user_id'];
			$_SESSION['name'] = $row[0]['users_fullname']." ".$row['last_name'];
			$_SESSION['uname'] = $uname;
			$_SESSION['password'] = $password;
			$_SESSION['timeout'] = time();
			$_SESSION["loggedin"]=1;
			return true;
		} else {
			return false;
		}
	}
	function logOut(){
		$inactive = 3600;//10 min in sec default 

		$session_life = time() - $_SESSION['timeout'];
		
		if($session_life > $inactive)
		{  session_destroy(); header('Location:'.URL_PATH.'login.php'); }
	}
	function smplmail($files, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message){
		$uid = md5(uniqid(time()));
		
		$header = "From: ".$from_name." <".$from_mail.">\r\n";
		$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$header .= "This is a multi-part message in MIME format.\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$header .= $message."\r\n\r\n";
		
			foreach ($files as $filename) { 
		
				$file = $path.$filename;
				$name = basename($file);
				//echo $name;
				$file_size = filesize($file);
				$handle = fopen($file, "r");
				$content = fread($handle, $file_size);
				fclose($handle);
				$content = chunk_split(base64_encode($content));
		
				$header .= "--".$uid."\r\n";
				$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
				$header .= "Content-Transfer-Encoding: base64\r\n";
				$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
				$header .= $content."\r\n\r\n";
			}
		
		$header .= "--".$uid."--";
		if(mail($mailto, $subject, "", $header)){return ('Message has been sent');}else{return ('Message sending fail');}
		}	
		
	function smtpmail($arg){
		 
		$mail = new PHPMailer;
		 
		$mail->isSMTP();									// Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';						// Specify main and backup server
		$mail->SMTPAuth = true;								// Enable SMTP authentication
		$mail->Username = $arg['from'];						// SMTP username
		$mail->Password = $arg['email_pwd']; 				// SMTP password
		$mail->SMTPSecure = 'tls';							// Enable encryption, 'ssl' also accepted
		$mail->Port = 587;									// Set the SMTP port number - 587 for authenticated TLS
		$mail->setFrom($arg['from'], $arg['name']);		// Set who the message is to be sent from
		$mail->addAddress($arg['to'], '');					// Add a recipient
		$mail->WordWrap = 50;								// Set word wrap to 50 characters
		$mail->addAttachment($arg['file']);					// Add attachments
		$mail->isHTML(true);								// Set email format to HTML
		 
		$mail->Subject =  $arg['subject'];
		$mail->Body    = $arg['body'];
		 
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		 
		if(!$mail->send()) {
		   $err = 'Message could not be sent.<br>';
		   $err .= 'Mailer Error: ' . $mail->ErrorInfo;
		   return $err;
		   exit;
		}
		 
		return ('Message has been sent');
	}
	function recurse_rmdir($dir) {//------------------------Delete whole Directory
		foreach(scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir("$dir/$file")) self::recurse_rmdir("$dir/$file");
			else unlink("$dir/$file");
		}
		rmdir($dir);
	}
	function recurse_copy($src,$dst) { //------------------------copy whole Directory
		$dir = opendir($src); 
		@mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				
				if ( is_dir($src . '/' . $file) ) { 
					self::recurse_copy($src . '/' . $file,$dst . '/' . $file); $files[]=$file;
				} 
				else { 
					copy($src . '/' . $file,$dst . '/' . $file); $files[]=$file;
				} 
			} 
		} 
		closedir($dir); 
		return $files;
	} 
}
	$fn = new functions;

//require_once(TEMPLATE_PATH.DS.'Page.php');
//include( CLASS_PATH.DS1.'PrivilegedUser.php');
//include( CLASS_PATH.DS1.'Role.php');
?>
