<?php
class PrivilegedUser 
{
    private $roles;

    public function __construct() {
        
    }

    // override User method
    public static function getByUsername($username) {
        $sql = "SELECT * FROM v_users_master WHERE users_username = '".$username."'";
        $result = functions::raw_selectData($sql);

        if (!empty($result)) {
            $privUser = new PrivilegedUser();
            $privUser->user_id = $result[0]["users_id"];
            $privUser->username = $username;
            $privUser->password = $result[0]["users_pwd"];
            $privUser->email_addr = $result[0]["users_email"];
            $privUser->initRoles();
            return $privUser;
        } else {
            return false;
        }
    }

    // populate roles with their associated permissions
    protected function initRoles() {
        $this->roles = array();
        $sql = "SELECT t1.role_id, t2.role_name FROM v_user_role as t1
                JOIN v_roles as t2 ON t1.role_id = t2.role_id
                WHERE t1.user_id = '".$this->user_id."'";
        $results = functions::raw_selectData($sql);

        foreach($results as $row ) {
            $this->roles[$row["role_name"]] = Role::getRolePerms($row["role_id"]);
        }
    }

    // check if user has a specific privilege
    public function hasPrivilege($perm) {
        foreach ($this->roles as $role) {
            if ($role->hasPerm($perm)) {
                return true;
            }
        }
        return false;
    }
}