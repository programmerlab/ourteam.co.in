<?php
/*
menu array should be like this

 $items = array(
 	 'ul'=> array('css_class'=>"nav", 'css_id'=>"side-menu"),
 	 'li'=> array(
		     array('parent_id'=>"0", 'id'=>"1", 'menu'=>"aaa", 'css_class'=>"aaa",'css_id'=>"aaa","lurl"=>"#a"),
		     array('parent_id'=>"0", 'id'=>"2", 'menu'=>"bbb", 'css_class'=>"bbb",'css_id'=>"bbb","lurl"=>"#b"),
		     array('parent_id'=>"2", 'id'=>"3", 'menu'=>"ccc", 'css_class'=>"ccc",'css_id'=>"ccc","lurl"=>"#c"),
		     array('parent_id'=>"3", 'id'=>"4", 'menu'=>"ddd", 'css_class'=>"ddd",'css_id'=>"ddd","lurl"=>"#d"),
		     array('parent_id'=>"4", 'id'=>"5", 'menu'=>"eee", 'css_class'=>"eee",'css_id'=>"eee","lurl"=>"#e"),
		     array('parent_id'=>"0", 'id'=>"6", 'menu'=>"fff", 'css_class'=>"fff",'css_id'=>"fff","lurl"=>"#f"),
	 )
 );

*/
class menus
{
	public function main_menu($items){
				//id,	pid,	mtitle,	css_class,	css_id,	urls	published,	m_position
		echo "<ul class='".$items['ul']['css_class']. "' id='".$items['ul']['css_id']."'>";
		foreach($items['li'] as $item){
			if($item['pid'] == 0){
				if($item['urls']!="#"){$item['urls']=URL_PATH.$item['urls'];}
				echo "<li><a href='".$item['urls']."'>".$item['mtitle']."</a>";
				$id = $item['id'];
				self::sub($items, $id);
				echo "</li>";
			}
		}
		echo "</ul>";
	}
	private function sub($items, $id){
		$i=0;
		foreach($items['li'] as $item){if($item['pid'] == $id){$i++;}}
		if($i > 0){
			echo "<ul id='".$i."'>";
			foreach($items['li'] as $item){
				if($item['pid'] == $id){
					if($item['urls']!="#"){$item['urls']=URL_PATH.$item['urls'];}
					echo "<li css='".$item['css_class']. "' id='".$item['css_id']."'><a href='".$item['urls']."'>".$item['mtitle']."</a>";
					self::sub($items, $item['id']);
					echo "</li>";
				}
			}
			echo "</ul>";
		}
	}
}
$mn=new menus;	
?>

