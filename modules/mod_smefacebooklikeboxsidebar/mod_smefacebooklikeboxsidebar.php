<?php
/*------------------------------------------------------------------------
# mod_smefacebooklikeboxsidebar - SME Facebook Like Box Sidebar
# ------------------------------------------------------------------------
# @author - Social Media Extensions
# @copyright - All rights reserved by Social Media Extensions
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.socialmediaextensions.com/
# Technical Support:  admin@socialmediaextensions.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die;
require JModuleHelper::getLayoutPath('mod_smefacebooklikeboxsidebar', $params->get('layout', 'default'));