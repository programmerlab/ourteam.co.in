<?php
/*------------------------------------------------------------------------
# mod_smefacebooklikeboxsidebar - SME Facebook Like Box Sidebar
# ------------------------------------------------------------------------
# @author - Social Media Extensions
# @copyright - All rights reserved by Social Media Extensions
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.socialmediaextensions.com/
# Technical Support:  admin@socialmediaextensions.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die;
$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_smefacebooklikeboxsidebar/assets/style.css');
//all parameters
$pageURL = $params->get('pageURL');
$connections = $params->get('connections');
$width = $params->get('width');
$height = $params->get('height');
$streams = $params->get('streams');
$color_scheme = $params->get('color_scheme');
$show_faces = $params->get('show_faces');
$header = $params->get('header');
$border = $params->get('border');
$marginTop = trim($params->get('marginTop'));
$sidebarImage = $params->get('sidebarImage');
$jQuery = trim($params->get('jQuery'));
if ($jQuery == 1){
$document->addScript("http://code.jquery.com/jquery-latest.min.js");}
$print_facebook = '';
$print_facebook .= '<div class="fb-page" data-href="'. $pageURL .'" data-width="'.$width.'" data-height="'.$height.'" data-hide-cover="'.$header.'" data-show-facepile="'.$show_faces.'" data-show-posts="'.$streams.'"><div class="fb-xfbml-parse-ignore"><blockquote cite="'. $pageURL .'"><a href="'. $pageURL .'">Facebook</a></blockquote></div></div>';
$print_facebook .= '';
?>
<div id="sw_facebook_display" class="<?php echo $params->get('moduleclass_sfx');?>">
	<div id="fbbox1" style="right: -<?php echo trim($width+10);?>px; top: <?php echo $marginTop;?>px; z-index: 10000; height:<?php echo trim($height);?>px;">
		<div id="fbbox2" style="text-align: left;width:<?php echo trim($width);?>px;height:<?php echo trim($height);?>px;">
			<a class="open" id="fblink" href="#"></a><img style="top: 0px;left:-40px;" src="modules/mod_smefacebooklikeboxsidebar/assets/<?php echo $sidebarImage;?>.png" alt="">
			
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <?php echo $print_facebook; ?>
                            
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery.noConflict();
jQuery(function (){
jQuery(document).ready(function()
{
jQuery.noConflict();
jQuery(function (){
jQuery("#fbbox1").hover(function(){ 
jQuery('#fbbox1').css('z-index',101009);
jQuery(this).stop(true,false).animate({right:  0}, 500); },
function(){ 
	jQuery('#fbbox1').css('z-index',10000);
	jQuery("#fbbox1").stop(true,false).animate({right: -<?php echo trim($width+10); ?>}, 500); });
});}); });
jQuery.noConflict();
</script>
