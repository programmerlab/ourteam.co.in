<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_otportfoliomanager
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.path');
jimport('joomla.image');
/**
 * Helper for mod_otportfoliomanager
 *
 * @package     Joomla.Site
 * @subpackage  mod_otportfoliomanager
 */
abstract class modOTPortfolioManagerHelper
{
	public static function getList(&$params)
	{
		
		/* $app = JFactory::getApplication();

		//	Retrieve Content
		$items = $model->getItems();

		 */
		$show_introtext = $params->get('show_introtext', 1);
		$introtext_limit = $params->get('introtext_limit', 0);$db = JFactory::getDbo();
		// Get a db connection.
		 
		// Create a new query object.
		$query = $db->getQuery(true);
		 
		// Select all records from the user profile table where key begins with "custom.".
		// Order it by the ordering field.
			$catids = $params->get('catid');
			$catids = implode(",",$catids);
		$query->select('*');
		$query->from($db->quoteName('#__otportfoliomanager_items'));
		$query->where($db->quoteName('categories') . ' IN ('.$catids.')');
		$query->order($params->get('ordering', 'id').' '.$params->get('article_ordering_direction', 'DESC')." ");
		$query->setLimit((int) $params->get('count', 5));
		 
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		 
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$items = $db->loadObjectList();
		foreach ($items as &$item)
		{
			$item->readmore = strlen(trim($item->description));
			$item->slug = $item->id;
			$item->catslug = $item->categories;
			// We know that user has the privilege to view the article
			$item->link = JRoute::_($item->slug, $item->categories);
			$item->linkText = JText::_('projectname');
			

			if ($show_introtext)
			{
				$item->introtext = JHtml::_('content.prepare', $item->description, '', 'mod_articles_news.content');
				$item->introtext = $introtext_limit ? self::_cleanIntrotext(self::truncate($item->introtext, $introtext_limit)) : $item->introtext;
			}
			$item->image2  = self::resize($item->image, 350, 350);

			//new
			if (!$params->get('image'))
			{
				$item->introtext = preg_replace('/<img[^>]*>/', '', $item->introtext);
			}
		}
		echo "<pre>";
		print_r($items);
		echo "</pre>";
		return $items;
	}
	public static function _cleanIntrotext($introtext)
	{
		$introtext = str_replace('<p>', ' ', $introtext);
		$introtext = str_replace('</p>', ' ', $introtext);
		$introtext = strip_tags($introtext, '<a><em><strong>');

		$introtext = trim($introtext);

		return $introtext;
	}
	/**
	* Method to truncate introtext
	*
	* The goal is to get the proper length plain text string with as much of
	* the html intact as possible with all tags properly closed.
	*
	* @param string   $html       The content of the introtext to be truncated
	* @param integer  $maxLength  The maximum number of charactes to render
	*
	* @return  string  The truncated string
	*/
	public static function truncate($html, $maxLength = 0)
	{
		$baseLength = strlen($html);

		// First get the plain text string. This is the rendered text we want to end up with.
		$ptString = JHtml::_('string.truncate', $html, $maxLength, $noSplit = true, $allowHtml = false);

		for ($maxLength; $maxLength < $baseLength;)
		{
			// Now get the string if we allow html.
			$htmlString = JHtml::_('string.truncate', $html, $maxLength, $noSplit = true, $allowHtml = true);

			// Now get the plain text from the html string.
			$htmlStringToPtString = JHtml::_('string.truncate', $htmlString, $maxLength, $noSplit = true, $allowHtml = false);

			// If the new plain text string matches the original plain text string we are done.
			if ($ptString == $htmlStringToPtString)
			{
				return $htmlString;
			}
			// Get the number of html tag characters in the first $maxlength characters
			$diffLength = strlen($ptString) - strlen($htmlStringToPtString);

			// Set new $maxlength that adjusts for the html tags
			$maxLength += $diffLength;
			if ($baseLength <= $maxLength || $diffLength <= 0)
			{
				return $htmlString;
			}
		}
		return $html;
	}
	static function resize($relativePath, $width = null, $height = null, $quality = null)
	{
		$absolutPath = JPath::clean(JPATH_ROOT.'/'.$relativePath);
		if (!JFile::exists($absolutPath))
		{
			JError::raiseWarning( 100, 'Image Path "'.$relativePath.'" could not be saved. Only images can be uploaded as logo.' );
			return $relativePath;
		}
		if ($width || $height)
		{
			$imageExt = JFile::getExt($absolutPath);
			$imageName = preg_replace('/\.'.$imageExt.'$/', '', JFile::getName($absolutPath));
			$imageNameNew = $imageName.'_w'.$width.'xh'.$height;
			$relativePathNew = str_replace($imageName.'.'.$imageExt, $imageNameNew.'.'.$imageExt, $relativePath);
			$absolutPathNew = JPath::clean(JPATH_ROOT.'/'.$relativePathNew);
			if (JFile::exists($absolutPathNew))
			{
				$imageDateOrigin = filemtime($absolutPath);
				$imageDateThumb = filemtime($absolutPathNew);
				$clearCache = ($imageDateOrigin > $imageDateThumb);
					if($clearCache == false)
					{
						return $relativePathNew;
					}
			}
			$image = new JImage($absolutPath);
			$properties = JImage::getImageFileProperties($absolutPath);
			if($properties->width < $width && $properties->height < $height)
			{
				return $relativePath;
			}
			$resizedImage = $image->resize($width, $height, true);
			$mime = $properties->mime;
			$options = array();
			if ($mime == 'image/jpeg')
			{
				$type = IMAGETYPE_JPEG;
				/*
				* from 0 (worst quality, smaller file) to 100 (best quality, biggest file)
				*/
				if ($quality)
				{
					$options['quality'] = $quality;
				}
				else
					{
						$options['quality'] = 75;
					}
			}
			elseif ($mime = 'image/png')
			{
				$type = IMAGETYPE_PNG;
				/*
				* Compression level: 0-9 or -1, where 0 is NO COMPRESSION at all,
				* 1 is FASTEST but produces larger files, 9 provides the best
				* compression (smallest files) but takes a long time to compress, and
				* -1 selects the default compiled into the zlib library.
				*/
				if ($quality)
				{
					$options['quality'] = $quality;
				}
				else
				{
					$options['quality'] = 6;
				}
			}
			elseif ($mime = 'image/gif')
			{
				$type = IMAGETYPE_GIF;
			}
			$resizedImage->toFile($absolutPathNew, $type, $options);
			return $relativePathNew;
		}
		return $relativePath;
	}
	/*
	* Method for image upload.
	*
	* Example usage: EasyImage::upload('logo', 200, 200);
	*
	* @param string Input name of the image
	* @param string Absolut path to upload folder
	* @return string Absolut path to uploaded image
	* @since 1.0.1
	*/
	static function upload($imageInputName, $uploadPath)
	{
		jimport('easy.file');
		return EasyFile::upload($imageInputName, $uploadPath, array('jpg', 'jpeg', 'png', 'gif'));
	}
}
