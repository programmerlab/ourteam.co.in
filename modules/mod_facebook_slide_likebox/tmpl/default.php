<?php 
/**
 * JS Social Tabs Slider
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @link       http://facebooklikebox.net
 */
 
	defined( '_JEXEC' ) or die( 'Restricted access' );	
	$document = & JFactory::getDocument();

	echo $slidelikebox;
	if (trim( $params->get( 'loadjquery' ) ) == 1) {
	$document->addScript("https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js");
	$document->addScript("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js");
	}
	else if (trim( $params->get( 'loadjquery' ) ) == 0) {
	$slidelikebox .= ' ';
					}
	/** Style 1 **/
if (trim( $params->get( 'fbstyle' ) ) == 1){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/sidebar/style'.'.css', 'text/css', null, array() ); 

if (trim( $params->get( 'position' ) ) == 0){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/sidebar/right'.'.css', 'text/css', null, array() ); 

}
else if (trim( $params->get( 'position' ) ) == 1){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/sidebar/left'.'.css', 'text/css', null, array() ); 

}
} 
/** Style 0 **/
if (trim( $params->get( 'fbstyle' ) ) == 0){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/slider/style'.'.css', 'text/css', null, array() ); 

if (trim( $params->get( 'position' ) ) == 0){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/slider/right'.'.css', 'text/css', null, array() ); 

}
else if (trim( $params->get( 'position' ) ) == 1){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/slider/left'.'.css', 'text/css', null, array() ); 

}
} 				
	
?>    