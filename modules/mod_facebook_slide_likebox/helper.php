﻿<?php
/**
 * Facebook Likebox Slider
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @link       http://facebooklikebox.net
 */
 
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

class modSlideLikebox {
	function getLikebox( $params )   {
		global $mainframe;
			
		if (trim( $params->get( 'position' ) ) == 1){	
					if (trim( $params->get( 'click' ) ) == 1)
					{?>
					<script type="text/javascript">
					    jQuery(function() {
					        var toggle;
					        jQuery(document).click(function()  {
					            if (toggle) {
					                toggle = !toggle;
					                jQuery(".social_slider")
					                    .stop(true, false)
					                    .animate({
					                        left: "-370"
					                    }, 1500, 'easeOutBounce');
					            }
					        });
					        jQuery(".tabs")
					            .click(function(e) {
					                e.stopPropagation();
					                toggle = !toggle;
					                if  (toggle) {
					                    jQuery('.social_slider')
					                        .stop(true, false)
					                        .animate({
					                            left: "0"
					                        }, 1500, 'easeInOutQuint');
					                }

					            });
					        $(".tabs")
					            .click(function(e) {
					                e.stopPropagation();
					            });

					    });
					</script>
					<?php
					}
else if  (trim( $params->get( 'click' ) ) == 0)
					{ ?>

					<script type="text/javascript">
					    jQuery(function() {
					        jQuery(".social_slider").hover(function() {
					                jQuery('.social_slider').stop(true, false).animate({
					                    left: "0"
					                }, 1500, 'easeInOutQuint');
					            },
					            function() {
					                jQuery(".social_slider").stop(true, false).animate({
					                    left: "-370"
					                }, 1500, 'easeOutBounce');
					            }, 1000);
					    });
					</script>
					<?php } ?>
<?php
}
else if (trim( $params->get( 'position' ) ) == 0){
if (trim( $params->get( 'click' ) ) == 1)
					{?>
					<script type="text/javascript">
					    jQuery(function() {
					        var toggle;
					        jQuery(document).click(function()  {
					            if (toggle) {
					                toggle = !toggle;
					                jQuery(".social_slider")
					                    .stop(true, false)
					                    .animate({
					                        right: "-370"
					                    }, 1500, 'easeOutBounce');
					            }
					        });
					        jQuery(".tabs")
					            .click(function(e) {
					                e.stopPropagation();
					                toggle = !toggle;
					                if  (toggle) {
					                    jQuery('.social_slider')
					                        .stop(true, false)
					                        .animate({
					                            right: "0"
					                        }, 1500, 'easeInOutQuint');
					                }

					            });
					        $(".tabs")
					            .click(function(e) {
					                e.stopPropagation();
					            });

					    });
					</script>
					<?php
					}
else if  (trim( $params->get( 'click' ) ) == 0)
					{ ?>

					<script type="text/javascript">
					    jQuery(function() {
					        jQuery(".social_slider").hover(function() {
					                jQuery('.social_slider').stop(true, false).animate({
					                    right: "0"
					                }, 1500, 'easeInOutQuint');
					            },
					            function() {
					                jQuery(".social_slider").stop(true, false).animate({
					                    right: "-370"
					                }, 1500, 'easeOutBounce');
					            }, 1000);
					    });
					</script>
					<?php } }?>
					
					<script type="text/javascript">
					    jQuery(document).ready(function() {
					        jQuery('.tabs .tab-links a').on('click', function(e) {
					            var currentAttrValue = jQuery(this).attr('href');

					            jQuery('.tabs ' + currentAttrValue).fadeIn(800).siblings().hide();
					       
					            jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

					            e.preventDefault();
					        });
					    });
					</script>

<?php 

if (trim( $params->get( 'fbstyle' ) ) == 0){
	?>
<div class="social_slider" style="top: <?php echo $params->get('margintop') ?>px !important;">
<?php } else if (trim( $params->get( 'fbstyle' ) ) == 1){ ?>
<div class="social_slider" style="top: 0px !important;">
<?php } ?>
<div class="tabs">
    <div class="tab-links">
      <?php

if (trim($params->get('facebook')) == 1)
	{ ?><a href="#tab1"><div class="facebook_icon"></div></a><?php
	} ?>
      <?php

if (trim($params->get('twitter')) == 1)
	{ ?><a href="#tab2"><div class="twitter_icon"></div></a><?php
	} ?>

    </div>

    <div class="tab-content">
      <?php

if (trim($params->get('facebook')) == 1)
	{ ?>   
        <div id="tab1" class="tab active">
         <div class="facebook_box">
          
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2F<?php echo $params->get('profile_id') ?>&amp;width=350&amp;height=<?php echo $params->get('height') ?>&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=<?php echo $params->get('stream') ?>&amp;show_border=true&amp;appId=317152851811780" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:<?php echo $params->get('height') ?>px; width: 350px;" allowTransparency="true"></iframe>

		 </div></div>
 	   <?php
	}

if (trim($params->get('twitter')) == 1)
	{ ?>
        <div id="tab2" class="tab <?php
	if (trim($params->get('facebook')) == 0)
		{ ?>active<?php
		} ?>">
	<?php
		if (trim( $params->get( 'fbstyle' ) ) == 0){
	?>
<div class="twitter_box" style="height:<?php echo $params->get('height') ?>px !important;">
<?php } else {?>
<div class="twitter_box">
<?php } ?>
           <a class="twitter-timeline" width="360px" height="<?php echo $params->get('height') ?>px" data-theme="light"  href="https://twitter.com/<?php
	echo $params->get('twitter_login'); ?>" data-widget-id="<?php
	echo $params->get('twwidgetid'); ?>">Tweets by @<?php
	echo $params->get('twitter_login'); ?></a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div></div></div></div></div>
	   <?php
	} 			
	}
}
?>