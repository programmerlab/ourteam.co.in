<?php 
/* 
* @author Bryan Keller
* Email : satechheads@yahoo.com
* URL : www.sanantoniocomputerrepair.net
* Description : This module displays icon links to your social media profiles.
* Copyright (c) 2008-2010 Techheads IT Consulting
* License GNU GPL
***/

// no direct access
defined('_JEXEC') or die;
 
$document =& JFactory::getDocument();
$mod = JURI::base() . 'modules/mod_socialmediapresence/';
$document->addStyleSheet(JURI::base() . 'modules/mod_socialmediapresence/style.css');

// Get Basic Module Parameters 
	$moduleclass_sfx 	= $params->get('moduleclass_sfx','');
	$target 			= $params->get('target','_blank');
	$robots			= $params->get('robots','1');
	$size 			= $params->get('size','32'); 
	$align 			= $params->get('align','left'); 
	$margin			= $params->get('margin','3px'); 
	$margin         = intval($margin);
	$text 		= $params->get('text','Follow us on'); 
	$rsstext 		= $params->get('rsstext','Subscribe to our Feed'); 
	$support      	= $params->get('support','1'); 
	$size2 	     	= intval($size) + intval($margin);
	$effect		= $params->get('effect','none');
	$ssize		= intval($size) - 4;
	$ssizeoffset	= intval($size) - 2 + intval($margin);
	$gsize		= intval($size) + 4;
	$gsizeoffset	= intval($size) + 2 + intval($margin);
	$backcolor		= $params->get('backcolor','#ffffff');
	$shiftoffset	= intval($size) + intval($margin);
	$highlight		= $params->get('highlight','#ffff00');
	$rotatedeg		= $params->get('rotatedeg',"90");

	$padbottom		= $params->get('padbottom', '10');
	$orientation	= $params->get('orientation', 'right');
	$toporbot		= $params->get('toporbot', 'bottom');
	$padrorl		= $params->get('padrorl', 'bottom');
	$startpos		= $params->get('startpos', 'side');
	$positioning	= $params->get('positioning', 'fixed');
	$backdrop		= $params->get('backdrop');
	$backdropcol	= $params->get('backdropcol');
	$bkdropheight	= $params->get('bkdropheight');
	$bkdropwidth	= $params->get('bkdropwidth');
	$bkdropcss		= $params->get('bkdropcss');
	$bkdropborder	= $params->get('bkdropborder');
	$bkdropborwidth	= $params->get('bkdropborwidth');
	$sidebar		= $params->get('sidebar');
	$css2			= $params->get('css2');
	$alignment		= $params->get('alignment');
	$opacity		= $params->get('opacity');

	$iconori = $padrorl;

	if($orientation == 'left'){
		if($startpos == 'center'){
			$iconori = "50%; margin-left:-" . $padrorl; 
			}
	}else{
		if($startpos == 'center'){
			$iconori = "50%; margin-right:-" . $padrorl; 
			}		
		}

// Prepare the Link Attribute
	if($robots == '1') {
	$nofollow = 'rel="nofollow"';
	}else{
	$nofollow = '';
	}

// Prepare the Icon Alignment Style
	$alignstyle = "text-align: $align ";

// Get Icon Parameters
$ic = array(
	$params->get('ic1'), $params->get('ic2'), $params->get('ic3'));


$url = array(
	$params->get('url1'), $params->get('url2'), $params->get('url3'));

$ht = array(
	$params->get('ht1'), $params->get('ht2'), $params->get('ht3'));

	
	$vimg = array();
     $vurl = array();

if($opacity == '100'){
		$opac = 'opacity:1.0;
filter:alpha(opacity=100);';
	}else{
		$opac = 'opacity:0.'.$opacity.';
filter:alpha(opacity='.$opacity.');';
	}


if ($sidebar == 'yes') {
		$sidestyle = 'position:'. $positioning .';'. $orientation .':'. $iconori .'px;'. $toporbot .':' .$padbottom. 'px; z-index:1000;';
	}else{
		$sidestyle = '';
	}

if ($alignment == 'horizontal') {
		$alignment = 'float: left;';
	}else{
		$alignment = '';
	}
	
switch ($effect) {
	case "zoom":
		$hoverclass="zoom";
 		break;
	case "shrink":
		$hoverclass="shrink";
     		break;
	case "bounce":
		$hoverclass="bounce";
		break;
	case "tilt":
		$hoverclass="tilt";
        	break;
	case "left":
		$hoverclass="left";
		break;
	case "right":
		$hoverclass="right";
      	break;
	case "down":
		$hoverclass="down";
		break;
	case "rotate90":
		$hoverclass="rotate90";
		break;
	case "rotate180":
		$hoverclass="rotate180";
		break;
	case "rotate270":
		$hoverclass="rotate270";
		break;
	case "rotatemin90":
		$hoverclass="rotatemin90";
		break;
	case "rotatemin180":
		$hoverclass="rotatemin180";
		break;
	case "rotatemin270":
		$hoverclass="rotatemin270";
		break;
}

// Set Wrapping Div
	echo '<div style="'.$sidestyle.'"><div class="soclinks" style="'.$alignstyle.'"><div> ';

	
// Prepare the Icon List
	for($i=0;$i < count($ic);$i++)
     {   
     $vimg[$ic[$i]]= htmlspecialchars($url[$i]);
	$vurl[$url[$i]]=$ic[$i];
	if($ht[$i] != ''){
			$text = $ht[$i];
		}else{
			$text = $text;
		}

	$title = ucwords(substr($vurl[$url[$i]], 0 , -4));


// Output the Icon Links	
	 	 if(($vimg[$ic[$i]]) != '') {
			echo '<div class="'.$hoverclass.' pic" style="'.$alignment.' z-index:1000;">';
			if($vurl[$url[$i]] == 'googleplus.png'){		 					$vimg[$ic[$i]] = 'https://plus.google.com/' . $vimg[$ic[$i]]. '?prsrc=3';
				}
			echo '<a '. $nofollow .' href="'. $vimg[$ic[$i]]. '" target="'. $target .'"><img style="'.$opac.' width: '.$size.'px; height: '.$size.'px; margin:'. $margin .'px;" src="'. $mod .'icons/'. $vurl[$url[$i]] .'" alt="'. $title .'" '; if($title == 'Feed') { echo 'title="'. $rsstext .'" /></a>';}else{ echo 'title="'. $text .' '. $title .'" /></a>';}

		echo '</div>';
	 }
 } 

echo '<div style="clear: both;"></div>';

echo '<div style="margin-left: 10px; text-align: center; font-size: 10px; color: #999999;">';

		if ($support == 'yes') echo '<p><a style="color: #999999;" href="http://www.frontallobemarketing.com/" title="San Antonio SEO">San Antonio SEO</a></p>';

echo '</div>';


		?>
	</div></div></div>
    <div class="clr"></div>