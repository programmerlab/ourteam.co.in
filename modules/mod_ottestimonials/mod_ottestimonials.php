<?php

/**

 * @copyright	@copyright	Copyright (c) 2014 OurTeam (www.ourteam.co.in). All rights reserved.

 * @license		GNU General Public License version 2 or later; see LICENSE.txt

 */



// no direct access

defined('_JEXEC') or die;



// include the syndicate functions only once

require_once dirname(__FILE__) . '/helper.php';



$list = ModOTTestimonialsHelper::getList($params);

$class_sfx = htmlspecialchars($params->get('class_sfx'));
$count1 = htmlspecialchars($params->get('count'));




require(JModuleHelper::getLayoutPath('mod_ottestimonials', $params->get('layout', 'default')));