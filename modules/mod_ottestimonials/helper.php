<?php

/**

 * @copyright	Copyright (c) 2014 OurTeam (www.ourteam.co.in). All rights reserved.

 * @license		GNU General Public License version 2 or later; see LICENSE.txt

 */



// no direct access

defined('_JEXEC') or die;



/**

 * OurTeam - OTTestimonials Helper Class.

 *

 * @package		Joomla.Site

 * @subpakage	testimonials.OTTestimonials

 */

class modOTTestimonialsHelper {

    public function getList($params)

	{

		$db = JFactory::getDBO();    



        $query  = $db->getQuery(true);

        $query->select('*')

         ->from('#__ottestimonials_ottestimonial')

         ->where("published != 0")

         ->order('id desc');

        $db->setQuery($query, 0, $params->get('count'));
		//die($params->get('count'));

        $result = $db->loadObjectList();

		return $result;

	}



}