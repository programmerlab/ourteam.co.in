/**
 * @copyright	Copyright (c) 2014 OurTeam (www.ourteam.co.in). All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
 var jQNC = jQuery.noConflict();
jQNC(document).ready(function($){
    $.fn.mstars = function() {
        return $(this).each(function() {
            // Get the value
            var val = parseFloat($(this).html());
            // Make sure that the value is in 0 - 5 range, multiply to get width
            var size = Math.max(0, (Math.min(5, val))) * 16;
            // Create stars holder
            var $span = $('<span />').width(size);
            // Replace the numerical value with stars
            $(this).html($span);
        });
    }
    $('span.mstars').mstars();
});
    
