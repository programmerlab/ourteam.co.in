<?php

/**

 * @copyright	Copyright (c) 2014 OurTeam (www.ourteam.co.in). All rights reserved.

 * @license		GNU General Public License version 2 or later; see LICENSE.txt

 */



// no direct access

defined('_JEXEC') or die;



JHtml::_('script', 'mod_ottestimonials/script.js', array(), true);

JHtml::_('stylesheet', 'mod_ottestimonials/style.css', array(), true);

?>

<div id="ottestimonials-ottestimonials">

	<?php foreach($list as $item){ ?>

    <div class="ottestimonial">

		<?php

		if(empty($item->alias)){

			$item->alias = $item->clientname;

		};

		$item->alias = JFilterOutput::stringURLSafe($item->alias);

		$item->linkURL = JRoute::_('index.php?option=com_ottestimonials&view=ottestimonial&id='.$item->id.':'.$item->alias);

		?>

        <blockquote class="mot_testimonial"><p><strong><?php echo $item->title; ?></strong><br><?php echo $item->description; ?></p></blockquote>

        <div class="arrow-down"></div>

		<p class="mot_testimonial-author">

            <strong><?php echo $item->clientname; ?></strong>

        </p>

		<p><span class="mstars"><?php echo $item->rate; ?></span></p>

		

        </div>

	<?php }; ?>
    <?php if($params->get('readmore_testimonial')!=""){?><p><a href="<?php echo JRoute::_($params->get('readmore_testimonial'));?>"> Read More...</a></p><?php }?>
    <?php if($params->get('add_testimonial')!=0){?>   <a href='index.php?option=com_ottestimonials&task=create' class="submit_link">&nbsp;</a><?php }?>

</div>

