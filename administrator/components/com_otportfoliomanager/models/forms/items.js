/**
 *	Items : Validate
 *	Filename : items.js
 *
 *	Author : Vishal Dubey
 *	Component : OT Portfolio Manager
 *
 *	Copyright : Copyright (C) 2014. All Rights Reserved
 *	License : GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
 *
 **/
window.addEvent('domready', function() {
	document.formvalidator.setHandler('projectname',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('categories',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
});