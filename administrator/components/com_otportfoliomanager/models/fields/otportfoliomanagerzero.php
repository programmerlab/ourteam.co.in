<?php
/*------------------------------------------------------------------------
# otportfoliomanagerzero.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * projectname Form Field class for the Otportfoliomanager component
 */
class JFormFieldotportfoliomanagerzero extends JFormFieldList
{
	/**
	 * The projectname field type.
	 *
	 * @var		string
	 */
	protected $type = 'otportfoliomanagerzero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__otportfoliomanager_items.id as id, #__otportfoliomanager_items.projectname as projectname');
		$query->from('#__otportfoliomanager_items');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->projectname));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>