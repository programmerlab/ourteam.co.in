<?php
/*------------------------------------------------------------------------
# otportfoliomanagerone.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * description Form Field class for the Otportfoliomanager component
 */
class JFormFieldotportfoliomanagerone extends JFormFieldList
{
	/**
	 * The description field type.
	 *
	 * @var		string
	 */
	protected $type = 'otportfoliomanagerone';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__otportfoliomanager_option.id as id, #__otportfoliomanager_option.description as description');
		$query->from('#__otportfoliomanager_option');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->description));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>