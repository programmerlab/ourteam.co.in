<?php
/*------------------------------------------------------------------------
#  - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * Ot_portfolio_manager Category Select Form Field class for the Otportfoliomanager component
 */
class JFormFieldotportfoliomanagercategoryzero extends JFormFieldList
{
	/**
	 * The otportfoliomanagercategoryzero field type.
	 *
	 * @var		string
	 */
	protected $type = 'otportfoliomanagercategoryzero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__categories.id as id, #__categories.title as title');
		$query->from('#__categories');
		$query->where('extension="com_otportfoliomanager"');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		$options[] = JHtml::_('select.option', 'all', 'All');
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->title));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>