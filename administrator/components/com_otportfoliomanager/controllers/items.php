<?php
/*------------------------------------------------------------------------
# items.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * Ot_portfolio_manager Controller Items
 */
class OtportfoliomanagerControlleritems extends JControllerForm
{
	public function __construct($config = array())
	{
		$this->view_list = 'otportfoliomanager'; // safeguard for setting the return view listing to the main view.
		parent::__construct($config);
	}

	/**
	 * Function that allows child controller access to model data
	 * after the data has been saved.
	 * 
	 * @param   JModel  &$model     The data model object.
	 * @param   array   $validData  The validated data.
	 * 
	 * @return  void
	 * 
	 * @since   11.1
	 */
	protected function postSaveHook(JModelLegacy &$model, $validData = array())
	{
		// Get a handle to the Joomla! application object
		$application = JFactory::getApplication();

		$user = JFactory::getUser();
		if($validData['user_created'] == 0){
			$data['user_created'] = $user->id;
		}
		$data['user_modified'] = $user->id;

		// Delete Image Checked
		if(array_key_exists('image1_delete', $_POST)){
			$data['image1'] = ''; // set image to nothing in database
			// Delete Image Entirely Check
			$db = JFactory::getDBO();
			$query = $db->getQuery(true)->select('image1')->from('#__otportfoliomanager_items')->where('id="' . $validData['id'] . '"');
			$db->setQuery((string)$query);
			$image1 = $db->loadResult();
			if($image1){
				$query = $db->getQuery(true)->select('CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END')->from('#__otportfoliomanager_items')->where('image1="' . $image1 . '" AND id!="' . $validData['id'] . '"');
				$db->setQuery((string)$query);
				$using = $db->loadResult();
				if($using == 0){ // free to delete
					// Include file system helpers
					jimport('joomla.filesystem.file');
					jimport('joomla.filesystem.folder');
					$full_image = JPATH_SITE . DS . 'images' . DS . 'com_otportfoliomanager' . DS . $image1;
					$thum_image = JPATH_SITE . DS . 'images' . DS . 'com_otportfoliomanager' . DS . 'thumb' . DS . $image1;
					JFile::delete($thum_image);
					if(JFile::delete($full_image)){
						// Add a message to the message queue
						$application->enqueueMessage('Image has been deleted!', 'notice');
					} else {
						// Add a message to the message queue
						$application->enqueueMessage('Image could not be deleted, but was removed from this item.', 'error');
					}
				} else {
					// Add a message to the message queue
					$application->enqueueMessage('Image has been removed from this item, but can not be deleted because it is being used elsewhere.', 'notice');
				}
			}
		}

		// Upload Image
		$file = JRequest::getVar('jform', array(), 'files', 'array');
		if($file['name']['image1']){
			$info = OtportfoliomanagerHelper::imageUpload($file, $data, 'image1');
			if($info['error'] == 0){
				$data['image1'] = $info['image1'];
			} else {
				// Add a message to the message queue
				$application->enqueueMessage($info['msg'], 'error');
			}
		}

		// Delete Image Checked
		if(array_key_exists('image2_delete', $_POST)){
			$data['image2'] = ''; // set image to nothing in database
			// Delete Image Entirely Check
			$db = JFactory::getDBO();
			$query = $db->getQuery(true)->select('image2')->from('#__otportfoliomanager_items')->where('id="' . $validData['id'] . '"');
			$db->setQuery((string)$query);
			$image2 = $db->loadResult();
			if($image2){
				$query = $db->getQuery(true)->select('CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END')->from('#__otportfoliomanager_items')->where('image2="' . $image2 . '" AND id!="' . $validData['id'] . '"');
				$db->setQuery((string)$query);
				$using = $db->loadResult();
				if($using == 0){ // free to delete
					// Include file system helpers
					jimport('joomla.filesystem.file');
					jimport('joomla.filesystem.folder');
					$full_image = JPATH_SITE . DS . 'images' . DS . 'com_otportfoliomanager' . DS . $image2;
					$thum_image = JPATH_SITE . DS . 'images' . DS . 'com_otportfoliomanager' . DS . 'thumb' . DS . $image2;
					JFile::delete($thum_image);
					if(JFile::delete($full_image)){
						// Add a message to the message queue
						$application->enqueueMessage('Image has been deleted!', 'notice');
					} else {
						// Add a message to the message queue
						$application->enqueueMessage('Image could not be deleted, but was removed from this item.', 'error');
					}
				} else {
					// Add a message to the message queue
					$application->enqueueMessage('Image has been removed from this item, but can not be deleted because it is being used elsewhere.', 'notice');
				}
			}
		}

		// Upload Image
		$file = JRequest::getVar('jform', array(), 'files', 'array');
		if($file['name']['image2']){
			$info = OtportfoliomanagerHelper::imageUpload($file, $data, 'image2');
			if($info['error'] == 0){
				$data['image2'] = $info['image2'];
			} else {
				// Add a message to the message queue
				$application->enqueueMessage($info['msg'], 'error');
			}
		}
		
		if(isset($validData['categories'])){
			$data['categories'] = implode(',', $validData['categories']);
		}

		$model->save($data);

	}

}
?>