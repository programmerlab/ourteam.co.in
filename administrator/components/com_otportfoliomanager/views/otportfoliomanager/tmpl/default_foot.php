<?php
/*------------------------------------------------------------------------
# default_foot.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<tr>
	<td colspan="5"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>