<?php
/*------------------------------------------------------------------------
# default_body.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$edit = "index.php?option=com_otportfoliomanager&view=otportfoliomanager&task=items.edit";
$user = JFactory::getUser();
$userId = $user->get('id');

// Connect to database
$db = JFactory::getDBO();
?>
<?php foreach($this->items as $i => $item){
	
	$canCheckin	= $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
	$userChkOut	= JFactory::getUser($item->checked_out);
	$categoryTitle = $db->setQuery('SELECT #__categories.id as cid1, #__categories.title as ctitle1 FROM #__categories WHERE #__categories.id IN ('.$item->categories.')')->loadObjectList();
	?>
	<tr class="row<?php echo $i % 2; ?>">
		<td>
			<?php echo $item->id; ?>
		</td>
		<td>
			<?php echo JHtml::_('grid.id', $i, $item->id); ?>
		</td>
		<td>
			<?php echo $item->projectname; ?> - (<a href="<?php echo $edit; ?>&id=<?php echo $item->id; ?>"><?php echo 'Edit'; ?></a>)
			<?php if ($item->checked_out){ ?>
				<?php echo JHtml::_('jgrid.checkedout', $i, $userChkOut->name, $item->checked_out_time, 'otportfoliomanager.', $canCheckin); ?>
			<?php } ?>
		</td>
		<td>
        <?php foreach($categoryTitle as $ct){?>
			<a href="index.php?option=com_categories&task=category.edit&id=<?php echo $ct->cid1; ?>&extension=com_otportfoliomanager"><?php echo $ct->ctitle1; ?></a>&nbsp;&nbsp;
        <?php }?>
		</td>
		<td><img src="<?php echo JURI::root()."images/com_otportfoliomanager/thumb/". $item->image1; ?> " alt="<?php echo $item->image1; ?>"  title="<?php echo $item->image1; ?>" style="width:40px;height:auto;">
		</td>
	</tr>
<?php } ?>