CREATE TABLE IF NOT EXISTS `#__otportfoliomanager_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectname` varchar(256) NOT NULL,
  `categories` int(11) NOT NULL DEFAULT '0',
  `description` TEXT NOT NULL DEFAULT '',
  `image1` varchar(256) NOT NULL,
  `image2` varchar(256) NOT NULL,
  `user_created` int(11) NOT NULL DEFAULT '0',
  `user_modified` int(11) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `#__otportfoliomanager_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(256) NOT NULL,
  `user_created` int(11) NOT NULL DEFAULT '0',
  `user_modified` int(11) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;