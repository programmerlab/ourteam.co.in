CREATE TABLE IF NOT EXISTS `#__ottestimonials_ottestimonial` (
 
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientname` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `published` tinyint(4) DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;