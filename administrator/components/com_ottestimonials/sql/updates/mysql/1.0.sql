CREATE TABLE IF NOT EXISTS `#__ottestimonials_ottestimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientname` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `title` varchar(256) NOT NULL,
  `description` TEXT NOT NULL DEFAULT '',
  `date_created` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;