<?php
/*------------------------------------------------------------------------
# ottestimonialszero.php - OT Testimonials Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014 OurTeam. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * clientname Form Field class for the Ottestimonials component
 */
class JFormFieldottestimonialszero extends JFormFieldList
{
	/**
	 * The clientname field type.
	 *
	 * @var		string
	 */
	protected $type = 'ottestimonialszero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__ottestimonials_ottestimonial.id as id, #__ottestimonials_ottestimonial.clientname as clientname');
		$query->from('#__ottestimonials_ottestimonial');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->clientname));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>