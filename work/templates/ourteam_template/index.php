<?php
defined( '_JEXEC' )or die;

include_once JPATH_THEMES . '/' . $this->template . '/logic.php';
// Get active template path from anywhere on Joomla: 
?>
<?php $path = JURI::root()."templates/".$this->template.''; ?>

<!doctype html>
<html lang="<?php echo $this->language; ?>" class="no-js">

<head>
	<script src="<?php echo $tpath;?>/js/jquery.js" type="text/javascript"></script>
	<jdoc:include type="head"/>
	<link href="<?php echo $tpath;?>/css/style.css" type="text/css" media="all" rel="stylesheet">
	<link href="<?php echo $tpath;?>/css/custom.css" type="text/css" media="all" rel="stylesheet">
	<link rel="stylesheet" id="kreme-google-fonts-css" href="//fonts.googleapis.com/css?family=Dosis%3A400%2C200%2C300%2C500%2C600%2C700%2C800%7CKameron%3A400%2C700%7CRaleway%3A400%2C100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C900italic%2C900%2C800italic%7CDancing+Script%3A400%2C700&amp;ver=1.0.0" type="text/css" media="all">
</head>

<body class="vc_responsive <?php echo $active->alias . ' ' . $pageclass; ?>">
	<div id="wrap" class="">
		<header id="header" class="header style-v1">
			<div class="header-inner">
			<?php if ($this->countModules('phone')||($this->countModules('top_right'))){?>
				<div class="header-top">
					<div class="header-top-inner">
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-6 header-top-left">
									<div class="sidebar-inner">
										<div id="widget-phone-info-2" class="widget widget_phone_info">
											<jdoc:include type="modules" name="phone" style="none"/>
										</div>
									</div>
								</div>
								<div class="col-sm-6 header-top-right">
									<div class="sidebar-inner">
											<jdoc:include type="modules" name="top_right" style="none"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><?php }?>
				<div class="header-middle">
					<div class="header-middle-inner">
						<div class="container-fluid">
							<div id="logo" class="logo">
								<jdoc:include type="modules" name="logo" style="none"/>
							
							</div>
							<?php if ($this->countModules('main_menu_1')||($this->countModules('main_menu_2'))){?>
							<nav id="primary-navigation" class="navbar" role="navigation">
								<div class="navbar-inner">
									<div id="navbar" class="">
										<ul id="menu-left-menu-1" class="nav navbar-nav"><li><jdoc:include type="modules" name="main_menu_1" style="none"/></li></ul>
										<ul id="menu-right-menu" class="nav navbar-nav main-menu-v2"><li><jdoc:include type="modules" name="main_menu_2" style="none"/></li></ul>
									</div>
								</div>
							</nav><?php } ?>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="main-content" class="main-content style-v1">
			<div class="main-contents-inner style-v1">
				<div class="page-content full-width">
					<div class="page-content-inner">
						<div class="container-fluid" style="padding: 0px;">
							<div class="page-content">
								<div class="row">
									<div class="col-sm-12">
										<div class="contents">
											<div class="page-entry">
												<?php if ($this->countModules('revslider')){?>
                                                <div class="revslider_in">
												<div class="wpb_wrapper">
													<jdoc:include type="modules" name="revslider" style="none"/>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="wrap-radius vc_row-no-padding revslider_in_clone">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<div class="section-radius-bottom section-radius "></div>
															</div>
														</div>
													</div>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												</div>
												<?php }?>
                        </div></div></div></div></div></div>                        
                        <div class="container-fluid">
							<div class="page-content">
								<div class="row">
									<div class="col-sm-12">
										<div class="contents">
											<div class="page-entry">
												<div class="vc_row wpb_row vc_row-fluid section-welcome section">
													<jdoc:include type="message" />
                                                    <jdoc:include type="component" />
												</div>
												<?php if ($this->countModules('content_bottom')){?>
												<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<jdoc:include type="modules" name="content_bottom" style="none"/>
														</div>
													</div>
												</div>
												
												<div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('content_bottom1')){?>
												<div class="vc_row wpb_row vc_row-fluid section section-nutrition-philosophy">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<jdoc:include type="modules" name="content_bottom1" style="none"/>
															</div>
														</div>
													</div>
												</div>
												<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid wrap-radius vc_row-no-padding">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<div class=" section-radius "></div>
															</div>
														</div>
													</div>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('testimonials_slider')){?>
												<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid section-testimonials section vc_custom_1487495132748 vc_row-has-fill">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
															<jdoc:include type="modules" name="testimonials_slider" style="none"/>
																
															</div>
														</div>
													</div>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('Why_choose_us')){?>
												<div class="vc_row wpb_row vc_row-fluid section">
													<jdoc:include type="modules" name="Why_choose_us" style="none"/>
													
												</div><div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('hot_deal')&& $this->countModules('get_a_coupon')){?>
												<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid section section-custom">
													<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<div class="hot-detail">
																	<div class="entry">
																		<jdoc:include type="modules" name="hot_deal" style="none"/>
																	</div>
																</div>
															</div>
														</div>
													</div>												
													<div class="contact-box wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<div class="contact-box-inner get_a_coupon1">
																	<div class="entry">
																		<jdoc:include type="modules" name="get_a_coupon" style="none"/>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												
												<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid wrap-radius vc_row-no-padding">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
																<div class="section-radius-bottom section-radius "></div>
															</div>
														</div>
													</div>
												</div>
												<div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('clients_logo_slider')){?>
												<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid section-clients-logo">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
															<jdoc:include type="modules" name="clients_logo_slider" style="none"/>
																
															</div>
														</div>
													</div>
												</div>
												
												<div class="vc_row-full-width vc_clearfix"></div>
												<?php }?>
												<?php if ($this->countModules('blog_section')){?>
												<div class="vc_row wpb_row vc_row-fluid section">
													<div class="wpb_column vc_column_container vc_col-sm-12">
														<div class="vc_column-inner ">
															<div class="wpb_wrapper">
															<jdoc:include type="modules" name="blog_section" style="none"/>
															</div>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer id="footer" class="footer style-v1">
		<?php if ($this->countModules('fmenu1')&& $this->countModules('fmenu2')&& $this->countModules('fmenu3')&& $this->countModules('fmenu4')){?>
			<div class="footer-info">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-3">
							<div class="sidebar-inner">
									<jdoc:include type="modules" name="fmenu1" style="footstyle"/>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="sidebar-inner">
								<jdoc:include type="modules" name="fmenu2" style="footstyle"/>

							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="sidebar-inner">
								<jdoc:include type="modules" name="fmenu3" style="footstyle"/>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="sidebar-inner">
								<jdoc:include type="modules" name="fmenu4" style="footstyle"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="footer-button">
				<div class="container">
					<div class="row">
						<?php if ($this->countModules('subscribe1')){?>
						<div class="col-sm-12 col-lg-8 subscribe">
							<div class="sidebar-inner">
								<jdoc:include type="modules" name="subscribe1" style="none"/>
							</div>
						</div>
						<?php }?>
						<?php if ($this->countModules('footer_social')){?>
						<div class="col-sm-12 col-lg-4">
							<div class="sidebar-inner">
								<jdoc:include type="modules" name="footer_social" style="none"/>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<?php if ($this->countModules('navbar_footer')){?>

						<div class="col-lg-6 col-lg-push-6 visible-lg">
							<div class="navbar-footer">
									<jdoc:include type="modules" name="navbar_footer" style="none"/>
							</div>
						</div>
												<?php }?>
						<?php if ($this->countModules('copyright')){?>
						<div class="col-lg-6 col-lg-pull-6">
							<div class="copyright-content">
									<jdoc:include type="modules" name="copyright" style="none"/>
							</div>
						</div>						<?php }?>

					</div>
				</div>
			</div>
		</footer>
	</div>
	<?php if ($this->countModules('debug')){?>
	<jdoc:include type="modules" name="debug"/>
	<?php }?>
	<?php /*?><script>
		jQuery(document).ready(function($) {
			varwx=$('body').outerWidth( true )
			var offset;
			offset = $(".revslider_in_clone").offset().left;
			
			$(".revslider_in_clone").attr("style", "width:"+varwx+"px;"+"left:-"+offset+"px;");
		});
	</script><?php */?>
	<script src="<?php echo $path;?>/build/app.js"></script>
</body>
</html>