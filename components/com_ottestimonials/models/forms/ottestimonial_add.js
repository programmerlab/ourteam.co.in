/**
 *	ottestimonial : Validate
 *	Filename : ottestimonial.js
 *
 *	Author : Vishal Dubey
 *	Component : OT Testimonials
 *
 *	Copyright : Copyright (C) 2014 OurTeam. All Rights Reserved
 *	License : GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
 *
 **/
window.addEvent('domready', function() {
	document.formvalidator.setHandler('clientname',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('email',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('rate',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('title',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('description',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
});