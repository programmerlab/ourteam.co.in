<?php
/*------------------------------------------------------------------------
# ottestimonials.php - OT Testimonials Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014 OurTeam. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
/**
 * Ottestimonials Model
 */
class OttestimonialsModelottestimonials extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('*');
		// From the ottestimonials_ottestimonial table
        $query->where($db->quoteName('published')." != 0");
		$query->from('#__ottestimonials_ottestimonial');

		return $query;
	}
    public function getTable($type = 'ottestimonial', $prefix = 'OttestimonialsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}


}
?>