<?php

/*------------------------------------------------------------------------

# ottestimonials.php - OT Testimonials Component

# ------------------------------------------------------------------------

# author    Vishal Dubey

# copyright Copyright (C) 2014 OurTeam. All Rights Reserved

# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

# website   www.ourteam.co.in

-------------------------------------------------------------------------*/



// No direct access to this file

defined('_JEXEC') or die('Restricted access');



// Added for Joomla 3.0

if(!defined('DS')){

	define('DS',DIRECTORY_SEPARATOR);

};



// Set the component css/js

$document = JFactory::getDocument();



$document->addStyleSheet('components/com_ottestimonials/assets/css/ottestimonials.css');
$document->addScript('components/com_ottestimonials/assets/js/ottestimonials.js');



// Require helper file

JLoader::register('OttestimonialsHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'ottestimonials.php');



// import joomla controller library

jimport('joomla.application.component.controller');



// Get an instance of the controller prefixed by Ottestimonials

$controller = JControllerLegacy::getInstance('Ottestimonials');



// Perform the request task

$controller->execute(JRequest::getCmd('task'));



// Redirect if set by the controller

$controller->redirect();

?>