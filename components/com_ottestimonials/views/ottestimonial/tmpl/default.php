<?php

/*------------------------------------------------------------------------

# default.php - OT Testimonials Component

# ------------------------------------------------------------------------

# author    Vishal Dubey

# copyright Copyright (C) 2014 OurTeam. All Rights Reserved

# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

# website   www.ourteam.co.in

-------------------------------------------------------------------------*/



// No direct access to this file

defined('_JEXEC') or die('Restricted access');



?>



<div id="ottestimonials-content">

  <div class="ottestimonial">

    <blockquote class="ot_testimonial">

      <p><strong><?php echo $this->item->title; ?></strong><br>

        <?php echo $this->item->description; ?></p>

    </blockquote>

    <div class="arrow-down"></div>

    <p class="ot_testimonial-author"><?php echo $this->item->clientname; ?> | <?php echo $this->item->email; ?> | <span class="stars"><?php echo $this->item->rate; ?></span></p>

    <br />

  </div>

</div>
<script>
 var jQNC1 = jQuery.noConflict();

jQNC1(document).ready(function($){

    $.fn.stars = function() {

        return $(this).each(function() {

            // Get the value

            var val = parseFloat($(this).html());

            // Make sure that the value is in 0 - 5 range, multiply to get width

            var size = Math.max(0, (Math.min(5, val))) * 16;

            // Create stars holder

            var $span = $('<span />').width(size);

            // Replace the numerical value with stars

            $(this).html($span);

        });

    }

    $('span.stars').stars();

});

    

</script>
