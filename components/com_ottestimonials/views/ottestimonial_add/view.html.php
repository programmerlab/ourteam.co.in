<?php
/*------------------------------------------------------------------------
# view.html.php - OT Testimonials Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014 OurTeam. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Ottestimonial View class for the Ottestimonials Component
 */
class OttestimonialsViewottestimonial_add extends JViewLegacy
{
	// Overwriting JViewLegacy display method
	
    function display($tpl = null)
	{
		// Check for errors.
		
        if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		};

		// Display the view
		parent::display($tpl);
	}
}
?>