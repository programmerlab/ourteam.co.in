<?php

/*------------------------------------------------------------------------

# default.php - OT Testimonials Component

# ------------------------------------------------------------------------

# author    Vishal Dubey

# copyright Copyright (C) 2014 OurTeam. All Rights Reserved

# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

# website   www.ourteam.co.in

-------------------------------------------------------------------------*/



defined('_JEXEC') or die('Restricted access');



JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('behavior.tooltip');

JHtml::_('behavior.formvalidation');

JHtml::_('formbehavior.chosen', 'select');

JHtml::_('behavior.keepalive');

jimport('joomla.application.component.controllerform');

$form   =JForm::getInstance('ottestimonial_add',JPATH_COMPONENT. '/models/forms/'.'ottestimonial_add.xml');

$params = $form->getFieldsets('params');



?>

<ul class="nav nav-tabs hidden" >

	<li class="active"><a data-toggle="tab" href="#home">tab</a></li>

</ul>
<div id="ottestimonial_add">
<form action="<?php echo JRoute::_('index.php?option=com_ottestimonials&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

	<div class="row-fluid">

		<div class="span12 form-horizontal">

			<fieldset class="adminform">

				<legend><?php echo JText::_( 'Details' ); ?></legend>

				<div class="adminformlist">

					<?php foreach($form->getFieldset('details') as $field){ ?>

						<div>

							<?php echo $field->label; echo $field->input;?>

						</div>

						<div class="clearfix"></div>

					<?php }; ?>

				</div>

			</fieldset>

		</div>

	</div>

    <div class="btn-toolbar">

			<div class="btn-group">



				<button type="button" class="btn btn-primary" onclick="Joomla.submitbutton('save')">

					<span class="icon-ok"></span>&#160;<?php echo JText::_('JSAVE') ?>

				</button>

			</div>

			<div class="btn-group">

				<button type="button" class="btn" onclick="Joomla.submitbutton('cancel')">

					<span class="icon-cancel"></span>&#160;<?php echo JText::_('JCANCEL') ?>

				</button>

			</div>

		</div>

	<div>

    <input type="hidden" name="task" value="save" />

    <input type="hidden" name="controller" value="" />

		<?php echo JHtml::_('form.token'); ?>

	</div>

</form>
</div>
