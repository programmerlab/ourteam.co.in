<?php

/*------------------------------------------------------------------------

# default.php - OT Testimonials Component

# ------------------------------------------------------------------------

# author    Vishal Dubey

# copyright Copyright (C) 2014 OurTeam. All Rights Reserved

# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

# website   www.ourteam.co.in

-------------------------------------------------------------------------*/



// No direct access to this file

defined('_JEXEC') or die('Restricted access');

jimport('joomla.filter.output');

?>

<div id="ottestimonials-ottestimonials">

	<?php foreach($this->items as $item){ ?>

    <div class="ottestimonial">

		<?php

		if(empty($item->alias)){

			$item->alias = $item->clientname;

		};

		$item->alias = JFilterOutput::stringURLSafe($item->alias);

		$item->linkURL = JRoute::_('index.php?option=com_ottestimonials&view=ottestimonial&id='.$item->id.':'.$item->alias);

		?>

        <blockquote class="ot_testimonial"><p><strong><?php echo $item->title; ?></strong><br><?php echo $item->description; ?></p></blockquote>

        <div class="arrow-down"></div>

		<p class="ot_testimonial-author"><a href="<?php echo $item->linkURL; ?>"><?php echo $item->clientname; ?></a> | <?php echo $item->email; ?> | <span class="stars"><?php echo $item->rate; ?></span></p>

		<p><a href="<?php echo $item->linkURL; ?>"> Read More...</a></p>

		<br />

        </div>

	<?php }; ?>

</div>
<script>
 var jQNC1 = jQuery.noConflict();

jQNC1(document).ready(function($){

    $.fn.stars = function() {

        return $(this).each(function() {

            // Get the value

            var val = parseFloat($(this).html());

            // Make sure that the value is in 0 - 5 range, multiply to get width

            var size = Math.max(0, (Math.min(5, val))) * 16;

            // Create stars holder

            var $span = $('<span />').width(size);

            // Replace the numerical value with stars

            $(this).html($span);

        });

    }

    $('span.stars').stars();

});

    

</script>

