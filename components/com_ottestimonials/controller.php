<?php

/*------------------------------------------------------------------------

# controller.php - OT Testimonials Component

# ------------------------------------------------------------------------

# author    Vishal Dubey

# copyright Copyright (C) 2014 OurTeam. All Rights Reserved

# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

# website   www.ourteam.co.in

-------------------------------------------------------------------------*/



// No direct access to this file

defined('_JEXEC') or die('Restricted access');



// import Joomla controller library

jimport('joomla.application.component.controller');



/**

 * Ottestimonials Component Controller

 */

class OttestimonialsController extends JControllerLegacy

{

	function display($cachable = false, $urlparams = false)

	{

		// set default view if not set

		JRequest::setVar('view', JRequest::getCmd('view', 'Ottestimonials'));



		// call parent behavior

		parent::display($cachable);



		// set view

		$view = strtolower(JRequest::getVar('view'));



	}
	function testimonial($cachable = false, $urlparams = false)

	{

		// set default view if not set

		JRequest::setVar('view', JRequest::getCmd('view', 'Ottestimonials'));



		// call parent behavior

		parent::display($cachable);



		// set view

		$view = strtolower(JRequest::getVar('view'));



	}

    

	function create()

	{

		// set default view if not set

		JRequest::setVar('view', JRequest::getCmd('view', 'Ottestimonial_add'));



		// call parent behavior

		parent::display($cachable);



		// set view

		$view = strtolower(JRequest::getVar('view'));



	}

    function save()

	{

		// Check for request forgeries

		JRequest::checkToken() or jexit( 'Invalid Token' );

 

		// get the model

		$model =& $this->getModel();

 

		//get data from request

		$post = JRequest::get('post');

		$post['content'] = JRequest::getVar('content', '', 'post', 'string', JREQUEST_ALLOWRAW);

 

		// let the model save it

		if ($model->store($post)) {

			$message = JText::_('Success');

		} else {

			$message = JText::_('Error while saving');

			$message .= ' ['.$model->getError().'] ';

		}

		$this->setRedirect('index.php?option=com_ottestimonials', $message);

	}



	/**

	* Cancel, redirect to component

	*/

	function cancel()

	{

		$this->setRedirect('index.php?option=com_ottestimonials');

	}



}

?>