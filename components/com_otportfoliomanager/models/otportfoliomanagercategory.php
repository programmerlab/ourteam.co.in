<?php
/*------------------------------------------------------------------------
# otportfoliomanagercategory.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Ot_portfolio_manager Cateory Model for Otportfoliomanager Component
 */
class OtportfoliomanagerModelotportfoliomanagercategory extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$pk = JRequest::getvar('id', 'all');
		// Create a new query object.
		$db = JFactory::getDBO();
		$query1	= $db->getQuery(true);
		// Select some fields
		$query1->select('*');
		// From the products_product table
		$query1->from('#__otportfoliomanager_items');
		//print_r($pk);
		
		if($pk!='all'){
			$query1->where('FIND_IN_SET('.$pk.',categories)');
		}
		
		return $query1;
	}
	public function getCatQuery()
	{
		// Create a new query object.
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__categories.id as id, #__categories.title as title, #__categories.*');
		$query->from('#__categories');
		$query->where('extension="com_otportfoliomanager" AND published="1"');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		return $items;
	}
}
?>