<?php
/*------------------------------------------------------------------------
# default.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="otportfoliomanager-content">
	<p><strong>Project Name</strong>: <?php echo $this->item->projectname; ?></p>
	<p><strong>Categories</strong>: <?php echo $this->item->categories; ?></p>
	<p><strong>Description</strong>: <?php echo $this->item->description; ?></p>
	<?php if($this->item->image1){ ?>
		<p><strong>Image 1</strong>: <img src="images/com_otportfoliomanager/<?php echo $this->item->image1; ?>" /></p>
	<?php } ?>
	<?php if($this->item->image2){ ?>
		<p><strong>Image 2</strong>: <img src="images/com_otportfoliomanager/<?php echo $this->item->image2; ?>" /></p>
	<?php } ?>
</div>