<?php foreach($this->items as $item){ ?>

<p><strong>Description</strong>: <?php echo $item->description; ?></p>
<?php } ?>
<div class="ot-portfolio-container no-carousel filter-isotope infinity-container">
  <div class="ot-portfolio-filter-nav clearfix">
    <div class="ot-portfolio-filter-nav-list button-group js-radio-button-group  pull-left"> <span><a href="javascript:void(0)" data-filter=".A_str" class="btn btn-mini">A</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".B_str" class="btn btn-mini">B</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".C_str" class="btn btn-mini">C</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".D_str" class="btn btn-mini">D</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".E_str" class="btn btn-mini">E</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".F_str" class="btn btn-mini">F</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".G_str" class="btn btn-mini">G</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".H_str" class="btn btn-mini">H</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".I_str" class="btn btn-mini">I</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".J_str" class="btn btn-mini">J</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".K_str" class="btn btn-mini">K</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".L_str" class="btn btn-mini">L</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".M_str" class="btn btn-mini">M</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".N_str" class="btn btn-mini">N</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".O_str" class="btn btn-mini">O</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".P_str" class="btn btn-mini">P</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".Q_str" class="btn btn-mini">Q</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".R_str" class="btn btn-mini">R</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".S_str" class="btn btn-mini">S</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".T_str" class="btn btn-mini">T</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".U_str" class="btn btn-mini">U</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".V_str" class="btn btn-mini">V</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".W_str" class="btn btn-mini">W</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".X_str" class="btn btn-mini">X</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".Y_str" class="btn btn-mini">Y</a></span>&nbsp;<span><a href="javascript:void(0)" data-filter=".Z_str" class="btn btn-mini">Z</a></span>&nbsp; <span><a data-filter="*" class="btn btn-mini" href="javascript:void(0)">Show All</a></span> <span class="form-inline ot_search">
      <input type="text" placeholder="Search" autocomplete="off" style="width:120px;" class="search1 form-control ui-autocomplete-input" id="searchid" name="keyword">
      <div class="result" id="result" style="display:none;"></div>
      <div style="display:none;">
        <input type="submit" value="Go" class="jvbt">
      </div>
      <input type="hidden" id="jpath" value="/home/yugtechn/public_html/joomla3" class="jpath">
      <input type="hidden" id="jroot" value="http://yugtechno.com/joomla3/" class="jroot">
      </span> </div>
    <div class="dropdown pull-right"> <a href="#" data-toggle="dropdown" class="btn btn-mini dropdown-toggle"><i class="otportfolio-fa otportfolio-fa-sort"></i> Sort Portfolio</a>
      <ul class="ot-portfolio-filter-nav-list dropdown-menu">
        <li><a data-filter="*" class="current" href="javascript:void(0)">All</a></li>
        <li><a data-filter=".cat6" href="javascript:void(0)">cat6</a></li>
        <li><a data-filter=".cat4" href="javascript:void(0)">cat4</a></li>
        <li><a data-filter=".cat3" href="javascript:void(0)">cat3</a></li>
        <li><a data-filter=".cat2" href="javascript:void(0)">cat2</a></li>
        <li><a data-filter=".cat1" href="javascript:void(0)">cat1</a></li>
      </ul>
    </div>
    <!-- end .dropdown --> 
  </div>
  <!-- end .ot-portfolio-filter-nav -->
  <div class="stroes">
    <div data-mode="fitRows" class="ot-portfolio-projects stroes-list masonry-holder clearfix" style="position: relative; height: 284px;">
      <div class="stroes-item ot-portfolio-item ot-portfolio-col cat6 F_str  f-portfolio-1  media-desc-below" style="position: absolute; left: 0px; top: 31px; display: none;">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="f-Portfolio 1"><img width="150" height="150" alt="f-Portfolio 1" src="/joomla3/cache/com_otportfolio/western-digital-1tb-hd-7200rpm_150x150.jpg"></span></div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> f-Portfolio 1 </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
          
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup"> <a class="close" href="#">Close</a>
          <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="f-Portfolio 1"><img width="150" height="150" alt="f-Portfolio 1" src="/joomla3/cache/com_otportfolio/western-digital-500gb-hd-7200rpm_150x150.jpg"></span></div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <div class="stroes-item ot-portfolio-item ot-portfolio-col cat4 D_str  d-pepole4  media-desc-below" style="position: absolute; left: 241px; top: 31px; display: none;">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="d-pepole4"><img width="150" height="150" alt="d-pepole4" src="/joomla3/cache/com_otportfolio/1329311-tn_run_1415014603_150x150.jpg"></span></div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> d-pepole4 </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
          
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup"> <a class="close" href="#">Close</a>
          <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="d-pepole4"><img width="150" height="150" alt="d-pepole4" src="/joomla3/cache/com_otportfolio/intel-core-2-extreme-qx9775-3-20ghz-retail_150x150.jpg"></span></div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <div class="stroes-item ot-portfolio-item ot-portfolio-col cat4 C_str  c-pepole2  media-desc-below" style="position: absolute; left: 483px; top: 31px; display: none;">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="c-pepole2"><img width="150" height="150" alt="c-pepole2" src="/joomla3/cache/com_otportfolio/1329305-tn_smell_1415014603_150x150.jpg"></span></div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> c-pepole2 </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
          
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup"> <a class="close" href="#">Close</a>
          <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="c-pepole2"><img width="150" height="150" alt="c-pepole2" src="/joomla3/cache/com_otportfolio/intel-c2d-e8400-3-0ghz-retail_150x150.jpg"></span></div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <div class="stroes-item ot-portfolio-item ot-portfolio-col cat4 B_str  b-pepole1  media-desc-below" style="position: absolute; left: 0px; top: 31px;">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="b-pepole1"><img width="150" height="150" alt="b-pepole1" src="/joomla3/cache/com_otportfolio/1329299-tn_face_1415014602_150x150.jpg"></span></div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> b-pepole1 </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
          
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup"> <a class="close" href="#">Close</a>
          <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="b-pepole1"><img width="150" height="150" alt="b-pepole1" src="/joomla3/"></span></div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <div class="stroes-item ot-portfolio-item ot-portfolio-col cat4 A_str  a-people  media-desc-below" style="position: absolute; left: 0px; top: 31px; display: none;">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="a-people"><img width="150" height="150" alt="a-people" src="/joomla3/cache/com_otportfolio/1329293-tn_see_1415014602_150x150.jpg"></span></div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> a-people </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
          
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup"> <a class="close" href="#">Close</a>
          <div style="width:150px;max-width:100%;" class="ot-portfolio-img"><span title="a-people"><img width="150" height="150" alt="a-people" src="/joomla3/"></span></div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <div class="stamp-holder" style="display: none;"></div>
    </div>
    <!-- end .mb2-portfolio-projects --> 
  </div>
</div>
