<?php
/*------------------------------------------------------------------------
# default.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
		$app            = JFactory::getApplication();
		$params         = $app->getParams();	// Assign data to the view
		
?>
<h1 class="page-header"><?php echo $this->document->title; ?></h1>
<?php if ($params->get('title')!=""){?><h2><?php echo $params->get('title') ?></h2><?php }?>
<div id="category-otportfoliomanager-content">
<?php echo $this->loadTemplate($params->get('portfolio_style'));?>
</div>