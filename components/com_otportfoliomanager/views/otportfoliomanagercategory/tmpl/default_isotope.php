<?php
/*------------------------------------------------------------------------
# controller.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file

defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
JHtml::_('jquery.framework');
$document->addScript('components/com_otportfoliomanager/assets/js/jquery-ui.js');
$document->addScript('components/com_otportfoliomanager/assets/js/jquery.isotope.min.js');
$document->addScript('components/com_otportfoliomanager/assets/js/jquery.main.js');
$document->addScript('components/com_otportfoliomanager/assets/js/otportfolio.js');
$document->addScript('components/com_otportfoliomanager/assets/js/result.js');


$model = $this->getModel('otportfoliomanagercategory');
$cats = $model->getCatQuery();
?>
<div class="ot-portfolio-container no-carousel filter-isotope infinity-container">
  <!-- nav -->
  <div class="ot-portfolio-filter-nav clearfix">
    <div class="ot-portfolio-filter-nav-list button-group js-radio-button-group  pull-left">
	<?php
	foreach (range('A', 'Z') as $char)
	{
       ?>
        <span>
            <a href="javascript:void(0)" data-filter="<?php echo '.'.$char.'_str';?>" class="btn btn-mini"><?php echo $char;?></a>
        </span>
       <?php
    }
    ?>    
    <?php /*?><span class="form-inline ot_search">
      <input type="text" placeholder="Search" autocomplete="off" style="width:120px;" class="search1 form-control ui-autocomplete-input" id="searchid" name="keyword">
      <div class="result" id="result" style="display:none;"></div>
      <div style="display:none;">
        <input type="submit" value="Go" class="jvbt">
      </div>
      <input type="hidden" id="jpath" value="/home/yugtechn/public_html/joomla3" class="jpath">
      <input type="hidden" id="jroot" value="http://yugtechno.com/joomla3/" class="jroot">
      </span><?php */?>
    </div>
    <div class="dropdown pull-right"> <a href="#" data-toggle="dropdown" class="btn btn-mini dropdown-toggle"><i class="otportfolio-fa otportfolio-fa-sort"></i> Sort Portfolio</a>
        <ul class="ot-portfolio-filter-nav-list dropdown-menu">
            <li><a data-filter="*" class="current" href="javascript:void(0)">All</a></li>
			<?php foreach($cats as $c){?>
            <li><a data-filter="<?php echo ".cat_".$c->id; ?>" href="javascript:void(0)"><?php echo $c->title; ?></a></li>
			<?php }?>
        </ul>
    </div>
    <!-- end .dropdown --> 
  </div>
  <!-- end nav -->
  <div class="stroes">
    <div data-mode="fitRows" class="ot-portfolio-projects stroes-list masonry-holder clearfix">
	<?php 
    //stdClass Object ( [id] => 3 [projectname] => gg2 [categories] => 15,18 [description] => [image1] => slide1.png [image2] => slide8.png [user_created] => 917 [user_modified] => 917 [checked_out] => 0 [checked_out_time] => 0000-00-00 00:00:00 ) 1
	foreach($this->items as $item){
    ?>
	<?php $cats4cls=explode(",",$item->categories); ?>
      <div class="stroes-item ot-portfolio-item ot-portfolio-col <?php echo " cat_".(implode(" cat_",$cats4cls)); ?> <?php echo ucfirst($item->projectname[0].'_str');?>  media-desc-below">
        <div class="ot-portfolio-item-inner brand brand-opener clearfix">
          <div class="ot-portfolio-item-media ot-portfolio-media">
            <div class="ot-portfolio-item-media-inner clearfix">
              <div class="ot-portfolio-img" style="max-width:100%;">
                  <span title="<?php echo $item->projectname; ?>">
                      <img alt="<?php echo $item->projectname; ?>" src="<?php echo JURI::base(true).'/images/com_otportfoliomanager/thumb/'.$item->image1; ?>">
                  </span>
              </div>
            </div>
            <!-- end .ot-portfolio-item-media-inner --> 
          </div>
          <!-- end .ot-portfolio-item-thumb -->
          <div class="ot-portfolio-item-details ot-portfolio-details">
            <div class="ot-portfolio-item-details-inner clearfix">
              <h2 class="title ot-portfolio-item-title"> <?php echo $item->projectname; ?> </h2>
            </div>
            <!-- end .ot-portfolio-item-deatils-inner --> 
          </div>
          <!-- end .ot-portfolio-item-deatils --> 
        </div>
        <!-- end .ot-portfolio-item-inner -->
        <div class="popup">
        <a class="close" href="#">Close</a>
          <div style="max-width:100%;" class="ot-portfolio-img">
              <span title="<?php echo $item->projectname; ?>">
                  <img alt="<?php echo $item->projectname; ?>" src="<?php echo JURI::base(true).'/images/com_otportfoliomanager/'.$item->image2; ?>">
              </span>
          </div>
        </div>
      </div>
      <!-- end .ot-portfolio-item -->
      <?php } ?>
      <div class="stamp-holder"></div>
    </div>
    <!-- end .ot-portfolio-projects --> 
  </div>
</div>
