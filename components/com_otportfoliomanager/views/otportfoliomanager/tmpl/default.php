<?php
/*------------------------------------------------------------------------
# default.php - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Connect to database
$db = JFactory::getDBO();
jimport('joomla.filter.output');
?>
<div id="otportfoliomanager-otportfoliomanager">
	<?php foreach($this->items as $item){ ?>
		<?php
		$item->categories = $db->setQuery('SELECT #__categories.title FROM #__categories WHERE #__categories.id = "'.$item->categories.'"')->loadResult();
		if(empty($item->alias)){
			$item->alias = $item->projectname;
		};
		$item->alias = JFilterOutput::stringURLSafe($item->alias);
		$item->linkURL = JRoute::_('index.php?option=com_otportfoliomanager&view=items&id='.$item->id.':'.$item->alias);
		?>
		<p><strong>Project Name</strong>: <a href="<?php echo $item->linkURL; ?>"><?php echo $item->projectname; ?></a></p>
		<p><strong>Categories</strong>: <?php echo $item->categories; ?></p>
		<p><strong>Description</strong>: <?php echo $item->description; ?></p>
		<?php if($item->image1){ ?>
			<p><strong>Image 1</strong>: <img src="images/com_otportfoliomanager/thumb/<?php echo $item->image1; ?>" /></p>
		<?php } ?>
		<?php if($item->image2){ ?>
			<p><strong>Image 2</strong>: <img src="images/com_otportfoliomanager/thumb/<?php echo $item->image2; ?>" /></p>
		<?php } ?>
		<p><strong>Link URL</strong>: <a href="<?php echo $item->linkURL; ?>">Go to page</a> - <?php echo $item->linkURL; ?></p>
		<br /><br />
	<?php }; ?>
</div>
