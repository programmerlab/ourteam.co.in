/*------------------------------------------------------------------------
# result.js - OT Portfolio Manager Component
# ------------------------------------------------------------------------
# author    Vishal Dubey
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   www.ourteam.co.in
-------------------------------------------------------------------------*/
(function($) {	
	$(document).ready(function() {
$(".search1").keyup(function() 
{ 
var searchid = $(this).val();
var jpath = $('#jpath').val();
var jroot = $('#jroot').val();
var dataString = 'search='+ searchid+'&path='+jpath+'&root='+jroot;
if(searchid!='')
{
    $.ajax({
    type: "POST",
	
    url: 'ajax.php',
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e){ 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
})(jQuery);